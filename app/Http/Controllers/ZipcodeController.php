<?php

namespace App\Http\Controllers;

use App\Models\Zipcode;
use Illuminate\Http\Request;

class ZipcodeController extends Controller
{
    public function getZipCode($zipcode = "")
    {
        if (!empty($zipcode)) {
            $r = Zipcode::where("id", $zipcode)->value("codes");
            if ($r)
                return $r;

            abort(404);
        }
    }
}
